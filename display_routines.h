/*
 * display_routines.h
 *
 *  Created on: 18 lip 2020
 *      Author: kroda
 */

#ifndef DISPLAY_ROUTINES_H_
#define DISPLAY_ROUTINES_H_

#include "display_api.h"
#include "ssd_1306.h"
#include "i2c.h"
#include "display_gfx.h"
//#include "FixedSys_font.h"
#include "OcrB_font.h"
#include "spec_symbols.h"

#include <avr/io.h>


void display_setup()
{
 	reset_ssd1306();
	set_vcomh_deselect_level_ssd1306(2); //(4);
	set_precharge_period_ssd1306(2, 2); //(1, 15);
	set_charge_pump_ssd1306(1);
	on_ssd1306();

	clear_gfx();
	setDrawingModeText_gfx();

	setFont_gfx(OCRB16x24s23_ascii48_57, 16, 24, 48);
	//setFont_gfx(fixedsys8x16, 8, 16, 32);

	// disable reset
	PORTB = PORTB | (1);
}

void display_teardown() {

}


#endif /* DISPLAY_ROUTINES_H_ */
