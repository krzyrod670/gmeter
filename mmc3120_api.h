/*
 * mmc3120_api.h
 *
 *  Created on: 18 lip 2020
 *      Author: kroda
 */

#ifndef MMC3120_API_H_
#define MMC3120_API_H_


typedef uint16_t adc_t;

typedef struct {
	adc_t mmc_x;
	adc_t mmc_y;
	adc_t mmc_z;
} Mmc3120Sample_t;


void mmc3120_setup();
void mmc3120_teardown();

void mmc3120_initiateMeasurement();
void mmc3120_getSample(Mmc3120Sample_t *data);
void mmc3120_coilSet();
void mmc3120_coilReset();

#endif /* MMC3120_API_H_ */
