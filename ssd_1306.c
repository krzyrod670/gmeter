/*
 * spi.c
 *
 *  Created on: 27 pa� 2019
 *      Author: kroda
 */


//set your clock speed
#define F_CPU 8000000UL
//these are the include files. They are outside the project folder
#include <avr/io.h>
#include "ssd_1306.h"
#include "spi_api.h"
#include "data_align.h"


#ifdef SSD_1306_COMM_SPI
	// SPI communications

	#define SSD_1306_DEFINE_LSB0_FIRST // Least significant bit for SSD_1306 defined at pos 0, normal
	#define comm_ssd1306 comm_ssd1306_spi
#else
	// Manual port manipulations

	#define SSD_1306_DEFINE_MSB0_FIRST // Least significant bit for SSD_1306 defined at pos 0, inversed order
	#define comm_ssd1306 comm_ssd1306_port_io
#endif


unsigned char reverse_msb0(unsigned char b) {
   b = (b & 0xF0) >> 4 | (b & 0x0F) << 4;
   b = (b & 0xCC) >> 2 | (b & 0x33) << 2;
   b = (b & 0xAA) >> 1 | (b & 0x55) << 1;
   return b;
}

#ifdef SSD_1306_DEFINE_MSB0_FIRST
	#define SSD_1306_DATA_BYTE(k) reverse_msb0(k)
#else
	#define SSD_1306_DATA_BYTE(k) (k)
#endif

#ifdef SSD_1306_DEFINE_MSB0_FIRST
	#define SSD_1306_COMMAND_BYTE(k) BYTE_I_HEX_##k
#else
	#define SSD_1306_COMMAND_BYTE(k) BYTE_N_HEX_##k
#endif


#define SET_CONTRAST_CONTROL_CMD SSD_1306_COMMAND_BYTE(81)
#define ENTIRE_DISPLAY_ON_RESUME_CMD SSD_1306_COMMAND_BYTE(A4)
#define ENTIRE_DISPLAY_ON_CMD SSD_1306_COMMAND_BYTE(A5)
#define SET_NORMAL_DISPLAY_CMD SSD_1306_COMMAND_BYTE(A6)
#define SET_INVERSE_DISPLAY_CMD SSD_1306_COMMAND_BYTE(A7)
#define SET_DISPLAY_OFF_CMD SSD_1306_COMMAND_BYTE(AE)
#define SET_DISPLAY_ON_CMD SSD_1306_COMMAND_BYTE(AF)

#define SET_LOWER_COLUMN_START_ADDRESS_CMD SSD_1306_COMMAND_BYTE(00)
#define SET_HIGHER_COLUMN_START_ADDRESS_CMD SSD_1306_COMMAND_BYTE(10)
#define SET_MEMORY_ADDRESSING_MODE_CMD SSD_1306_COMMAND_BYTE(20)
#define SET_COLUMN_ADDRESS_CMD SSD_1306_COMMAND_BYTE(21)
#define SET_PAGE_ADDRESS_CMD SSD_1306_COMMAND_BYTE(22)
#define SET_PAGE_START_ADDRESS_CMD SSD_1306_COMMAND_BYTE(B0)

#define SET_PRE_CHARGE_PERIOD_CMD SSD_1306_COMMAND_BYTE(D9)
#define SET_VCOMH_DESELECT_LEVEL_CMD SSD_1306_COMMAND_BYTE(DB)
#define SET_CHARGE_PUMP_CMD SSD_1306_COMMAND_BYTE(8D)

#define PAGE_ADDRESSING_MODE_ARG SSD_1306_COMMAND_BYTE(02)
#define HORIZONTAL_ADDRESSING_MODE_ARG SSD_1306_COMMAND_BYTE(00)
#define VERTICAL_ADDRESSING_MODE_ARG SSD_1306_COMMAND_BYTE(01)

#define NOP_CMD SSD_1306_COMMAND_BYTE(E3)




void set_charge_pump_ssd1306(char enabled)
{
	if (enabled) {
		write_cmd_2_ssd1306(SET_CHARGE_PUMP_CMD, SSD_1306_DATA_BYTE(0x14));
	}
	else {
		write_cmd_2_ssd1306(SET_CHARGE_PUMP_CMD, SSD_1306_DATA_BYTE(0x10));
	}

}


void set_precharge_period_ssd1306(char p1, char p2)
{
	write_cmd_2_ssd1306(SET_PRE_CHARGE_PERIOD_CMD, SSD_1306_DATA_BYTE((p1 & 0x0F) | ((p2 & 0x0F) << 4)));

}

void set_vcomh_deselect_level_ssd1306(char level)
{
	write_cmd_2_ssd1306(SET_VCOMH_DESELECT_LEVEL_CMD, SSD_1306_DATA_BYTE((level & 0x07) << 4));
}

void set_inversed_display_ssd1306()
{
	write_cmd_ssd1306(SET_INVERSE_DISPLAY_CMD);
}

void set_non_inversed_display_ssd1306()
{
	write_cmd_ssd1306(SET_NORMAL_DISPLAY_CMD);
}

void set_contrast_ssd1306(char level)
{
	write_cmd_2_ssd1306(SET_CONTRAST_CONTROL_CMD, SSD_1306_DATA_BYTE(level));
}

void on_ssd1306()
{
	write_cmd_ssd1306(SET_DISPLAY_ON_CMD);
}

void sleep_ssd1306()
{
	write_cmd_ssd1306(SET_DISPLAY_OFF_CMD);
}

void entire_display_on_ssd1306()
{
	write_cmd_ssd1306(ENTIRE_DISPLAY_ON_CMD);
}

void entire_display_ram_ssd1306()
{
	write_cmd_ssd1306(ENTIRE_DISPLAY_ON_RESUME_CMD);
}


void setup_page_addressing_ssd1306()
{
	write_cmd_2_ssd1306(SET_MEMORY_ADDRESSING_MODE_CMD, PAGE_ADDRESSING_MODE_ARG);
}

void setup_horizontal_addressing_ssd1306()
{
	write_cmd_2_ssd1306(SET_MEMORY_ADDRESSING_MODE_CMD, HORIZONTAL_ADDRESSING_MODE_ARG);
}

void setup_vertical_addressing_ssd1306()
{
	write_cmd_2_ssd1306(SET_MEMORY_ADDRESSING_MODE_CMD, VERTICAL_ADDRESSING_MODE_ARG);
}


void set_page_pg_addressing_ssd1306(char page)
{
	write_cmd_ssd1306(SET_PAGE_START_ADDRESS_CMD | SSD_1306_DATA_BYTE((page & 0x07)));
}

void set_start_column_pg_addressing_ssd1306(char col)
{
	write_cmd_ssd1306(SET_LOWER_COLUMN_START_ADDRESS_CMD | SSD_1306_DATA_BYTE((col & 0x0F)));
	write_cmd_ssd1306(SET_HIGHER_COLUMN_START_ADDRESS_CMD | SSD_1306_DATA_BYTE(((col >> 4) & 0x07)));
}

void set_column_range_vh_addressing(char start_column, char end_column)
{
	write_cmd_ssd1306(SET_COLUMN_ADDRESS_CMD);
	write_cmd_ssd1306(SSD_1306_DATA_BYTE(start_column & 0x7F));
	write_cmd_ssd1306(SSD_1306_DATA_BYTE(end_column & 0x7F));
}

void set_page_range_vh_addressing(char start_page, char end_page)
{
	write_cmd_ssd1306(SET_PAGE_ADDRESS_CMD);
	write_cmd_ssd1306(SSD_1306_DATA_BYTE(start_page & 0x07));
	write_cmd_ssd1306(SSD_1306_DATA_BYTE(end_page & 0x07));
}


void reset_ssd1306() {

	// dc = high , cs low , reset to low
	control_ssd1306(1, 0, 0);

	delay_ms(250);

	// dc = high , cs high , reset to high
	control_ssd1306(1, 1, 1);
}




void write_cmd_ssd1306(char cmd)
{
	// dc = low , cs low , reset to high
	control_ssd1306(0, 0, 1);

	comm_ssd1306(cmd);

	// dc = high , cs high , reset to high
	control_ssd1306(1, 1, 1);
}


void write_cmd_2_ssd1306(char cmd_first, char cmd_second)
{
	// dc = low , cs low , reset to high
	control_ssd1306(0, 0, 1);

	comm_ssd1306(cmd_first);
	comm_ssd1306(cmd_second);

	// dc = high , cs high , reset to high
	control_ssd1306(1, 1, 1);
}

void write_cmd_3_ssd1306(char cmd_first, char cmd_second, char cmd_third)
{
	// dc = low , cs low , reset to high
	control_ssd1306(0, 0, 1);

	comm_ssd1306(cmd_first);
	comm_ssd1306(cmd_second);
	comm_ssd1306(cmd_third);

	// dc = high , cs high , reset to high
	control_ssd1306(1, 1, 1);
}

void write_data_ssd1306(char dat)
{
	// dc = high , cs low , reset to high
	control_ssd1306(1, 0, 1);

	transmit_spi(dat);

	// dc = high , cs high , reset to high
	control_ssd1306(1, 1, 1);
}

void write_data_array_ssd1306(char* dat, int len)
{
	// dc = high , cs low , reset to high
	control_ssd1306(1, 0, 1);

	while (len > 0) {
		comm_ssd1306(dat);
		--len;
	}

	// dc = high , cs high , reset to high
	control_ssd1306(1, 1, 1);
}


void comm_ssd1306_spi(char data_cmd)
{
	transmit_spi(data_cmd);
}

void comm_ssd1306_port_io(char data_cmd)
{
	// data_cmd must be in inversed order, MSB at pos 0
	for (int i = 0; i < 8; i++) {
		control_full_ssd1306(PORTB & (1 << SSD_1306_DC) ,0, 1, data_cmd & 0x01, 0);
		control_full_ssd1306(PORTB & (1 << SSD_1306_DC) ,0, 1, data_cmd & 0x01, 1);
		data_cmd >>= 1;
	}
}

void control_ssd1306(char dc, char cs, char res) {

	uint8_t port = PORTB & (~(1 << SSD_1306_DC) | (1 << SSD_1306_RES));
	port |= (dc << SSD_1306_DC) | (res << SSD_1306_RES);
	PORTB = port;

	//PORTB = (dc << SSD_1306_DC) | (cs << SSD_1306_CS) | (res << SSD_1306_RES);
}


void control_full_ssd1306(char dc, char cs, char res, char sdin, char sck) {

	PORTB = (dc << SSD_1306_DC) | (cs << SSD_1306_CS) | (res << SSD_1306_RES) |  (sdin << SSD_1306_SDIN) | (sck << SSD_1306_SCK) ;
}


void setup_atmel_ssd() {

	control_ssd1306(1, 1, 1); // dc = high, cs = high, res = high

	DDRB = 0xFF; // PB0..PB7 are outputs

}


