/*
 * display_gfx.h
 *
 *  Created on: 30 lis 2019
 *      Author: kroda
 */

#ifndef DISPLAY_GFX_H_
#define DISPLAY_GFX_H_

#include <stdint.h>

// Drawing options - see drawString_gfx

#define DRAW_OPTION_ANCHOR_X_LEFT 0x00
#define DRAW_OPTION_ANCHOR_X_CENTER 0x01
#define DRAW_OPTION_ANCHOR_X_RIGHT 0x03

/** Clears the screen */
void clear_gfx();





/**
 *  end values are exclusive
 */
void fill_pattern_gfx(char value, char start_col, char end_col, char start_page, char end_page);

void setFont_gfx(const char glcdbitmaps[], char glyph_width, char glyph_height, char first_ascii);

void revertFont_gfx();

int16_t printInteger_gfx(int integer, uint8_t page, int16_t column, uint8_t options);

int16_t drawString_gfx(const char* text, uint8_t page, int16_t column, uint8_t options);

int16_t drawSymbol_gfx(char c, const char *symbols, unsigned char page, int16_t column, char page_height);

/**
 * Calculates the width, in pixels, of a given string and for the current font
 */
int16_t getStringWidth_gfx(const char* text);

// returns: next column
char drawChar_gfx(const char ch, unsigned char page, unsigned char column);

uint8_t getSymbolWidth_gfx(char c, const char *symbols);

void fillRange_gfx(char c, uint8_t column_start, uint8_t page_start, uint8_t column_end, uint8_t page_end);

#endif /* DISPLAY_GFX_H_ */
