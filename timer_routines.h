/*
 * timer_routines.h
 *
 *  Created on: 18 lip 2020
 *      Author: kroda
 */

#ifndef TIMER_ROUTINES_H_
#define TIMER_ROUTINES_H_

#include "timer_api.h"
#include <avr/io.h>

void timer_setup()
{
	// enable timer overflow interrupt for both Timer1 and Timer0
	TCCR0 = (1 << CS02 | 1 << CS00);   // For main clock: Prescaler = System CLK/1024 = 7812,5 Hz
	TCCR1B |= 1 << WGM12 |  1 << CS10; // 8MHz
	OCR1A = 500;
	TIMSK = 0; // both timer interrupts are stopped (1 << OCIE1A | 1 << TOIE0);
}

void timer_teardown()
{
	TIMSK = 0;
}

void timer_mainStart()
{
	TCNT0 = 176;

	// enable timer 0 overflow interrupt
	TIMSK = TIMSK | 1 << TOIE0;
}

void timer_mainStop()
{
	TIMSK = TIMSK & (~(1 << TOIE0));
}

// must be invoked from timer interrupt as first instruction
void timer_mainSchedule()
{
	TCNT0 = 176;
}

void timer_precisionStart()
{
	// enable timer 1 output compare interrupt
	TIMSK = TIMSK | 1 << OCIE1A;
}

void timer_precisionStop()
{
	TIMSK = TIMSK & (~(1 << OCIE1A));
}


#endif /* TIMER_ROUTINES_H_ */
