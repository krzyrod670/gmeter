/*
 * timer_api.h
 *
 *  Created on: 18 lip 2020
 *      Author: kroda
 */

#ifndef TIMER_API_H_
#define TIMER_API_H_

void timer_setup();
void timer_teardown();

void timer_mainStart();
void timer_mainStop();
void timer_mainSchedule();
void timer_precisionStart();
void timer_precisionStop();

#endif /* TIMER_API_H_ */
