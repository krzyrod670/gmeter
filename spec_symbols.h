/*
 * spec_symbols.h
 *
 *  Created on: 30 lis 2019
 *      Author: kroda
 */

#ifndef SPEC_SYMBOLS_H_
#define SPEC_SYMBOLS_H_


#include <avr/pgmspace.h>


// symbols
// for
// compass scale

const char busola_sym_pzl [] PROGMEM = {

		// offsets
		0x04, 0x0B, 0x0C, 0x0D

		// bitmap data
        , 0x80, 0xE0, 0xFC, 0xFF, 0xFC, 0xE0, 0x80 // Code for ^ , index = 0x04
		, 0xFF // Code for | , index = 0x0B
		, 0x80 // code for . , index - 0x0C
};

const char busola_sym_pzl_v2 [] PROGMEM = {

		// offsets
		0x04, 0x06, 0x07, 0x08

		// bitmap data
        , 0xFF, 0xFF// Code for || , index = 0x04
		, 0xF8 // Code for | , index = 0x06
		, 0x80 // code for . , index - 0x07
};

const char busola_sym_pzl_v3 [] PROGMEM = {

		// offsets
		0x04, 0x0B, 0x0D, 0x0F

		// bitmap data
        , 0x01, 0x07, 0x3F, 0xFF, 0x3F, 0x07, 0x01 // Code for ^ , index = 0x04
		, 0xFF , 0xFF // Code for | , index = 0x0B
		, 0xC0 , 0xC0// code for . , index - 0x0D
};
#endif /* OCRB_FONT_H_ */
