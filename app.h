/*
 * app.h
 *
 *  Created on: 6 sty 2020
 *      Author: kroda
 */

#ifndef APP_H_
#define APP_H_

#include <stdint.h>

struct
{
	uint16_t m_x, m_y, m_z;
	uint16_t azim;
	uint16_t errors;

} volatile asv;

// errors
#define MAGNETOMETER_ERROR_COMM 0x0001;
#define MAGNETOMETER_ERROR 0x0002;
#define I2C_ERROR 0x8000;


#endif /* APP_H_ */
