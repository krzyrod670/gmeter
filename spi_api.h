/*
 * spi_api.h
 *
 *  Created on: 27 pa� 2019
 *      Author: kroda
 */

#ifndef SPI_API_H_
#define SPI_API_H_


// any transmit was initiated
#define SPI_STATE_DIRTY 0x80
#define SPI_STATE_UNINITIALIZED 0x40

uint8_t spi_priorityQueue[10];
uint8_t spi_secondaryQueue[30];

volatile uint8_t spi_state;

void setup_spi();
void transmit_spi(uint8_t);
void waitTransmitted();

#endif /* SPI_API_H_ */
