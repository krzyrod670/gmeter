/*
 * i2c.c
 *
 *  Created on: 6 sty 2020
 *      Author: kroda
 */
#include <avr/io.h>

#include "i2c.h"
#include "display_gfx.h"

volatile uint8_t i2c_errors;


void wait_i2c_op();


void i2c_setup() {

	// i2c = 100 kHz
	TWBR = 32;
	TWSR = 0;

	i2c_errors = 0;
}


void i2c_start()
{

#ifdef I2C_DEBUG
	drawString_gfx("                       ", 0, 60, 0);
#endif

	// send START condition
	TWCR = (1 << TWINT) | (1 << TWSTA) | (1 << TWEN);

	wait_i2c_op();

	// check start was sent with code 0x08

#ifdef I2C_DEBUG
	if ((TWSR & 0xF8) == 0x08) {
		drawString_gfx("0.08", 0, 60, 0);
	}
	else {
		drawString_gfx("!0...", 0, 60, 0);
		i2c_errors |= I2C_START_ERROR;
	}
#else
	if ((TWSR & 0xF8) != 0x08) {
		i2c_errors |= I2C_START_ERROR;
	}
#endif
}

void i2c_rep_start()
{
	// send START condition
	TWCR = (1 << TWINT) | (1 << TWSTA) | (1 << TWEN);

	wait_i2c_op();

// check  repeated start was sent with code 0x10

#ifdef I2C_DEBUG
	if ((TWSR & 0xF8) == 0x10) {
		drawString_gfx("1.10", 0, 90, 0);
	}
	else  {
		drawString_gfx("!1..", 0, 90, 0);
		i2c_errors |= I2C_REP_START_ERROR;
	}
#else
	if ((TWSR & 0xF8) != 0x10) {
		i2c_errors |= I2C_REP_START_ERROR;
	}
#endif

}

void i2c_stop()
{
	// STOP
	TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWSTO);
}


void i2c_address_sla_w(uint8_t address)
{
	TWDR = (address & 0x7F) << 1; // SLA_W - bit 0 is cleared
	TWCR = (1 << TWINT) | (1 << TWEN);
	wait_i2c_op();

	// check code 0x18 was set

#ifdef I2C_DEBUG
	if ((TWSR & 0xF8) == 0x18) {
		drawString_gfx("2.18", 0, 60, 0);
	}
	else if ((TWSR & 0xF8) == 0x20) {
		drawString_gfx("!2.20", 0, 60, 0);
		i2c_errors |= I2C_SLA_W_ERROR;
	}
	else if ((TWSR & 0xF8) == 0x38) {
		drawString_gfx("!2.38", 0, 60, 0);
		i2c_errors |= I2C_SLA_W_ERROR;
	}
	else {
		drawString_gfx("!2...", 0, 60, 0);
		i2c_errors |= I2C_SLA_W_ERROR;
	}
#else
	if ((TWSR & 0xF8) != 0x18) {
		i2c_errors |= I2C_SLA_W_ERROR;
	}
#endif
}

void i2c_address_sla_r(uint8_t address)
{
	TWDR = ((address & 0x7F) << 1) | 0x01; // SLA_R - bit 0 is set
	TWCR = (1 << TWINT) | (1 << TWEN);
	wait_i2c_op();

	// check code 0x40 was set

#ifdef I2C_DEBUG
	if ((TWSR & 0xF8) == 0x40) {
		drawString_gfx("3.40", 0, 90, 0);
	}
	else if ((TWSR & 0xF8) == 0x48) {
		drawString_gfx("!3.48", 0, 90, 0);
		i2c_errors |= I2C_SLA_R_ERROR;
	}
	else if ((TWSR & 0xF8) == 0x38) {
		drawString_gfx("!3.38", 0, 90, 0);
		i2c_errors |= I2C_SLA_R_ERROR;
	}
	else {
		drawString_gfx("!3...", 0, 90, 0);
		i2c_errors |= I2C_SLA_R_ERROR;
	}
#else
	if ((TWSR & 0xF8) != 0x40) {
		i2c_errors |= I2C_SLA_R_ERROR;
	}
#endif
}

void i2c_write(uint8_t data)
{
	TWDR = data;
	TWCR = (1 << TWINT) | (1 << TWEN);
	wait_i2c_op();

	// check code 0x28 was set

#ifdef I2C_DEBUG
	if ((TWSR & 0xF8) == 0x28) {
		drawString_gfx("4.28", 0, 60, 0);
	}
	else if ((TWSR & 0xF8) == 0x30) {
		drawString_gfx("!4.30", 0, 60, 0);
		i2c_errors |= I2C_WRITE_ERROR;
	}
	else if ((TWSR & 0xF8) == 0x38) {
		drawString_gfx("!4.38", 0, 60, 0);
		i2c_errors |= I2C_WRITE_ERROR;
	}
	else {
		drawString_gfx("!4...", 0, 60, 0);
		i2c_errors |= I2C_WRITE_ERROR;
	}
#else
	if ((TWSR & 0xF8) != 0x28) {
		i2c_errors |= I2C_WRITE_ERROR;
	}
#endif

}

uint8_t i2c_read()
{
	TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWEA);
	wait_i2c_op();

	// check code 0x50 was set

#ifdef I2C_DEBUG
	if ((TWSR & 0xF8) == 0x50) {
		drawString_gfx("5.50", 0, 90, 0);
	}
	else if ((TWSR & 0xF8) == 0x58) {
		drawString_gfx("!5.58", 0, 90, 0);
		i2c_errors |= I2C_READ_ERROR;
	}
	else if ((TWSR & 0xF8) == 0x38) {
		drawString_gfx("!5.38", 0, 90, 0);
		i2c_errors |= I2C_READ_ERROR;
	}
	else {
		drawString_gfx("!5...", 0, 90, 0);
		i2c_errors |= I2C_READ_ERROR;
	}
#else
	if ((TWSR & 0xF8) != 0x50) {
		i2c_errors |= I2C_READ_ERROR;
	}
#endif

	return TWDR;
}

uint8_t i2c_read_end()
{
	TWCR = (1 << TWINT) | (1 << TWEN);
	wait_i2c_op();

	// check code 0x58 was set

#ifdef I2C_DEBUG
	if ((TWSR & 0xF8) == 0x58) {
		drawString_gfx("7.58", 0, 90, 0);
	}
	else if ((TWSR & 0xF8) == 0x50) {
		drawString_gfx("!7.50", 0, 90, 0);
		i2c_errors |= I2C_READ_END_ERROR;
	}
	else if ((TWSR & 0xF8) == 0x38) {
		drawString_gfx("!7.38", 0, 90, 0);
		i2c_errors |= I2C_READ_END_ERROR;
	}
	else {
		drawString_gfx("!7...", 0, 90, 0);
		i2c_errors |= I2C_READ_END_ERROR;
	}
#else
	if ((TWSR & 0xF8) != 0x58) {
		i2c_errors |= I2C_READ_END_ERROR;
	}
#endif

	return TWDR;
}



void wait_i2c_op()
{
	// wait for flag
	while (!(TWCR & (1 << TWINT))) ;
}
