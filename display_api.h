/*
 * display_api.h
 *
 *  Created on: 18 lip 2020
 *      Author: kroda
 */

#ifndef DISPLAY_API_H_
#define DISPLAY_API_H_

void display_setup();
void display_teardown();

#endif /* DISPLAY_API_H_ */
