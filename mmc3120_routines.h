/*
 * mmc3120_routines.h
 *
 *  Created on: 18 lip 2020
 *      Author: kroda
 */

#ifndef MMC3120_ROUTINES_H_
#define MMC3120_ROUTINES_H_

#include "mmc3120_api.h"

#include "app.h"
#include "i2c.h"
#include "display_gfx.h"


void mmc3120_setup()
{
	mmc3120_coilSet();
	delay_ms(50);
	mmc3120_coilReset();
	delay_ms(50);
}

void mmc3120_teardown()
{

}

void mmc3120_initiateMeasurement()
{
	i2c_errors = 0;

	// i2c Start
	i2c_start();

	// Write Address and start transmission
	if(!i2c_errors) i2c_address_sla_w(0x30);

	// Write  address of MMC3120 internal register (0)
	if(!i2c_errors) i2c_write(0x00);

	// Write value 0x01 - initiate measurement
	if(!i2c_errors) i2c_write(0x01);

	// i2c STOP
	if(!i2c_errors) i2c_stop();


	// magnetometer comm flag
	if (i2c_errors) {
		asv.errors |= MAGNETOMETER_ERROR_COMM;
		asv.errors |= MAGNETOMETER_ERROR;
	}
	else {
		asv.errors &= ~MAGNETOMETER_ERROR_COMM;
		asv.errors &= ~MAGNETOMETER_ERROR;
	}
}


void mmc3120_getSample(Mmc3120Sample_t *data)
{
	uint8_t *byteData = (uint8_t*) data;

	i2c_errors = 0x00;

	// i2c start
	i2c_start();

	// Write Address and start transmission
	if (!i2c_errors) i2c_address_sla_w(0x30);

	// Write  address of MMC3120 internal register (0)
	if (!i2c_errors) i2c_write(0x00);

	// Read Address and start transmission
	if (!i2c_errors) i2c_rep_start();

	if (!i2c_errors) i2c_address_sla_r(0x30);

	if (!i2c_errors) i2c_read(); // #0

	if (!i2c_errors) byteData[1] = i2c_read(); // #1
	if (!i2c_errors) byteData[0] = i2c_read(); // #0

	if (!i2c_errors) byteData[3] = i2c_read(); // #3
	if (!i2c_errors) byteData[2] = i2c_read(); // #4

	if (!i2c_errors) byteData[5] = i2c_read(); // #5
	if (!i2c_errors) byteData[4] = i2c_read_end(); // last byte #6

	// send i2c STOP condition
	if (!i2c_errors) i2c_stop();

	// magnetometer comm flag
	if (i2c_errors) {
		asv.errors |= MAGNETOMETER_ERROR_COMM;
		asv.errors |= MAGNETOMETER_ERROR;
	}
	else {
		asv.errors &= ~MAGNETOMETER_ERROR_COMM;
		asv.errors &= ~MAGNETOMETER_ERROR;
	}

}


void mmc3120_coilSet()
{
	i2c_errors = 0;

	// i2c Start
	i2c_start();

	// Write Address and start transmission
	if(!i2c_errors) i2c_address_sla_w(0x30);

	// Write  address of MMC3120 internal register (0)
	if(!i2c_errors) i2c_write(0x00);

	// Write value 0x02 - set
	if(!i2c_errors) i2c_write(0x02);

	// i2c STOP
	if(!i2c_errors) i2c_stop();


	// magnetometer comm flag
	if (i2c_errors) {
		asv.errors |= MAGNETOMETER_ERROR_COMM;
		asv.errors |= MAGNETOMETER_ERROR;
	}
	else {
		asv.errors &= ~MAGNETOMETER_ERROR_COMM;
		asv.errors &= ~MAGNETOMETER_ERROR;
	}

}

void mmc3120_coilReset()
{
	i2c_errors = 0;

	// i2c Start
	i2c_start();

	// Write Address and start transmission
	if(!i2c_errors) i2c_address_sla_w(0x30);

	// Write  address of MMC3120 internal register (0)
	if(!i2c_errors) i2c_write(0x00);

	// Write value 0x04 - reset
	if(!i2c_errors) i2c_write(0x04);

	// i2c STOP
	if(!i2c_errors) i2c_stop();


	// magnetometer comm flag
	if (i2c_errors) {
		asv.errors |= MAGNETOMETER_ERROR_COMM;
		asv.errors |= MAGNETOMETER_ERROR;
	}
	else {
		asv.errors &= ~MAGNETOMETER_ERROR_COMM;
		asv.errors &= ~MAGNETOMETER_ERROR;
	}

}


#endif /* MMC3120_ROUTINES_H_ */
