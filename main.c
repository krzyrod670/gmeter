//set your clock speed
#define F_CPU 8000000UL
#define HEADLESS

//these are the include files. They are outside the project folder
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdint.h>

//this include is in your project folder
#include "abc.h"
#include "app.h"

#include "api.h"

#include "uart_routines.h"
#include "spi_routines.h"
#include "timer_routines.h"
#include "comm_routines.h"
#include "mxr2312el_routines.h"
#include "mmc3120_routines.h"

#ifndef HEADLESS
#include "led_routines.h"
#include "needle_routines.h"
#include "display_routines.h"
#endif


void rtCycleRoutine();

//---------------------------------------------------------------------------------------------------------------

typedef struct SampleDataVector
{
	Mxr2312Sample_t acc;
	Mmc3120Sample_t mag;
} SampleDataVector_t;


typedef struct MeasurementVector
{
	uint16_t acc_x; // acceleration along X axis of accelerometer, G; format 16.16
	uint16_t acc_y; // acceleration along Y axis of accelerometer, G; format 16.16
	uint16_t mag_x; // magnetic readout along X axis of magnetometer, Gauss; format 16.16
	uint16_t mag_y; // magnetic readout along Y axis of magnetometer, Gauss; format 16.16
	uint16_t mag_z; // magnetic readout along Z axis of magnetometer, Gauss; format 16.16
	uint16_t temp; // readout of temperature, Kelvin; format 10.6
} MeasurementVector_t;

typedef struct GMeterCalculatedDataVector
{
	uint16_t acc; // acceleration; format 8.8
	uint16_t bearing; //bearing , degrees 0-359; format 16.0
	uint16_t acc_vector_tilt; // acc. vertor tilt,degrees ; format 8.8
	uint16_t bearing_change; // change of bearing, degrees/sec ; format 8.8
	uint16_t temp; // temperature, Kelvin, 10.6
} GMeterCalculatedDataVector_t;

typedef struct CycleDataVector
{
	uint8_t sequence_number;
	SampleDataVector_t sample;
	MeasurementVector_t measurement;
	//GMeterCalculatedDataVector_t calculated;
	//uint32_t ext1;
	//uint32_t ext2;
} CycleDataVector_t;




typedef union CommData {
	uint8_t commData[256];
	CycleDataVector_t cycle;
} CommData_t;

//---------------------------------------------------------------------------------------------------------------


// timer0 overflow
ISR(TIMER0_OVF_vect) {
	timer_mainSchedule(); // schedule next interrupt
	rtCycleRoutine();
}

// timer1 compare
ISR(TIMER1_COMPA_vect) {
#ifndef HEADLESS
	update_needle();
#endif
}

//UART tx buffer empty
ISR(USART_UDRE_vect) {
	uart_continueBulkTransmit(); // continue with transmission of next byte from bulk transfer
}


//---------------------------------------------------------------------------------------------------------------

CommData_t data;

int main(void) {

	//-----------------------------------------------------
	// init hardware

	setup_spi();
	uart_setup();
	i2c_setup();
	timer_setup();
	comm_setup();
	mxr2312_setup();
	mmc3120_setup();

#ifndef HEADLESS
	setup_atmel_ssd();
	setup_led();
	display_setup();
#endif

	sei(); // enable interrupts

	//-----------------------------------------------------
	// init data

	for (int i = 0; i < sizeof(CommData_t); data.commData[i++] = 0x00);

	// ---------------------------------------------------------------------
	// initial cycle

	timer_mainStart();

	data.cycle.sequence_number = 0;

	// initiate first Magnetometer measurement for the initial RT cycle
	mmc3120_initiateMeasurement();

	// initiate first Accelerometer measurement for the initial RT cycle
	mxr2312_getSample(&data.cycle.sample.acc);

	//----------------------------------------------------------------------
	// synchronous cycle
	while(1);

	//----------------------------------------------------------------------
	// Exit

	timer_mainStop();
}

// Real-Time cycle routine
void rtCycleRoutine() {

	// obtain sample data from magnetometer; initiated on prev cycle
	mmc3120_getSample(&data.cycle.sample.mag);

	// calculate measurements from samples for prev. cycle
	// ...

	// calculate application data
	// ...

	// transmit data via COMM
	comm_sendFrame(data.commData, sizeof(CycleDataVector_t));

	//--------------------------------------------------------------------
	// next cycle

	// increment seq number
	++ data.cycle.sequence_number;

	mmc3120_initiateMeasurement();

	mxr2312_getSample(&data.cycle.sample.acc);
}

