/*
 * display_gfx.c
 *
 *  Created on: 30 lis 2019
 *      Author: kroda
 */

#include "display_gfx.h"

#include <avr/pgmspace.h>
#include "ssd_1306.h"

#define GFX_DISPLAY_WIDTH 128

#define DRAW_OPTION_ANCHOR_MASK 0x03

void clear_gfx()
{
	setup_horizontal_addressing_ssd1306();
	set_page_range_vh_addressing(0,7);
	set_column_range_vh_addressing(0,127);

	char i = 0xFF;
	do {
		write_data_ssd1306(0x00);
		write_data_ssd1306(0x00);
		write_data_ssd1306(0x00);
		write_data_ssd1306(0x00);
		write_data_ssd1306(0x00);
		write_data_ssd1306(0x00);
		write_data_ssd1306(0x00);
		write_data_ssd1306(0x00);
	} while (--i);
}

void fill_pattern_gfx(char value, char start_col, char end_col, char start_page, char end_page)
{
	setup_horizontal_addressing_ssd1306();
	set_page_range_vh_addressing(start_page, end_page -1);
	set_column_range_vh_addressing(start_col, end_col -1);

	int i =  end_col - start_col;
	int j = end_page - start_page;
	i *= j;
	do {
		write_data_ssd1306(value);
	} while (--i);
}

void setDrawingModeText_gfx()
{
	setup_vertical_addressing_ssd1306();
}

char p_font_stride = 0;
const char *p_font_glcdbitmaps = 0;
char p_font_first_ascii = 0;
char p_font_pages_height = 0;

char p_font_stride_stacked = 0;
const char *p_font_glcdbitmaps_stacked = 0;
char p_font_first_ascii_stacked = 0;
char p_font_pages_height_stacked = 0;


void setFont_gfx(const char glcdbitmaps[], char glyph_width, char glyph_height, char first_ascii)
{
	p_font_stride_stacked = p_font_stride;
	p_font_glcdbitmaps_stacked = p_font_glcdbitmaps;
	p_font_first_ascii_stacked = p_font_first_ascii;
	p_font_pages_height_stacked = p_font_pages_height;

	p_font_glcdbitmaps = glcdbitmaps;
	p_font_first_ascii = first_ascii;
	p_font_stride = (char) (((int)glyph_width * glyph_height) >> 3) + 1;
	p_font_pages_height = glyph_height >> 3;
}

void revertFont_gfx()
{
	p_font_stride = p_font_stride_stacked;
	p_font_glcdbitmaps = p_font_glcdbitmaps_stacked;
	p_font_first_ascii = p_font_first_ascii_stacked;
	p_font_pages_height = p_font_pages_height_stacked;
}


int16_t getStringWidth_gfx(const char* text)
{
	// calculate text width
	int16_t width = 0;
	for(char i = 0; text[i]; i++) {
		width += (int8_t) pgm_read_byte(p_font_glcdbitmaps + (text[i] - p_font_first_ascii) * p_font_stride);
	}

	return width;
}

int16_t printInteger_gfx(int integer, uint8_t page, int16_t column, uint8_t options)
{
	char text[] = "          ";
	char *p = text + 9;

	int k = abs(integer);

	do {
		char x = '0' + (char) (k%10);
		*(p--) = x;

		k = k / 10;
	} while (k);

	if (integer < 0) {
		*(p) = '-';
	} else p++;

	return drawString_gfx(p, page, column, options);
}


int16_t drawString_gfx(const char* text, uint8_t page, int16_t column, uint8_t options)
{

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Calculating start and end column of text

	// calculate text width
	int16_t text_end_col = 0;
	for(char i = 0; text[i]; i++) {
		text_end_col += (int8_t) pgm_read_byte(p_font_glcdbitmaps + (text[i] - p_font_first_ascii) * p_font_stride);
	}

	// column: first column on left (lowest column)
	// text_end_col: column past the last column on right (highest column + 1)

	// adjust right column based on options
	if ((options & DRAW_OPTION_ANCHOR_MASK) == DRAW_OPTION_ANCHOR_X_CENTER) {
		// centered anchor
		column -= text_end_col / 2;
	}
	else if ((options & DRAW_OPTION_ANCHOR_MASK) == DRAW_OPTION_ANCHOR_X_LEFT) {
		// left anchor
		// do nothing
	}
	else if ((options & DRAW_OPTION_ANCHOR_MASK) == DRAW_OPTION_ANCHOR_X_RIGHT) {
		// right anchor
		column -= text_end_col - 1;
	}

	// common
	text_end_col += column;

	//////////////////////////////////////////////////////////////////////////////////////////////////////

	// check if text fits within display boundaries
	if (column >=  GFX_DISPLAY_WIDTH || text_end_col <= 0) {
		return text_end_col;
	}


	const char * p = 0;
	int8_t c_width = 0;

	// skip beyond left boundary
	while (column < 0) {
		p = p_font_glcdbitmaps + (*(text++) - p_font_first_ascii) * p_font_stride;
		c_width = pgm_read_byte(p++);

		if (column + c_width <= 0) {
			column += c_width;
			c_width = 0;
		}
		else {
			column = abs(column);
			p += p_font_pages_height * column;
			c_width -= column;
			column = 0;
		}
	}

	// truncate end column
	int16_t end_col = text_end_col > GFX_DISPLAY_WIDTH ? GFX_DISPLAY_WIDTH : text_end_col;

	// set page range for string
	set_page_range_vh_addressing(page, page + p_font_pages_height - 1);

	// set column range for string
	set_column_range_vh_addressing(column, end_col - 1);

	do {
		column += c_width;

		while(c_width--) {

			for (char y = 0; y < p_font_pages_height; y++) {
				write_data_ssd1306(pgm_read_byte(p++));
			}
		}

		p = p_font_glcdbitmaps + (*(text++) - p_font_first_ascii) * p_font_stride;

		c_width = pgm_read_byte(p++);

		if (column + c_width > end_col) {
			c_width = (uint8_t)(end_col - column);
		}

	} while (column < end_col);

	return text_end_col;
}



char drawChar_gfx(const char ch, unsigned char page, unsigned char column)
{
	int addr = (ch - p_font_first_ascii) * p_font_stride;
	const char * p = p_font_glcdbitmaps + addr;

	char c_width = pgm_read_byte(p++);
	unsigned char end_col = column + c_width;
	if (end_col > 128) end_col = 128;


	// set page range for string
	set_page_range_vh_addressing(page, page + p_font_pages_height - 1);
	// set column range for string
	set_column_range_vh_addressing(column, end_col - 1);


	// iterator
	column = end_col - column;

	while(column) {
		for (char y = 0; y < p_font_pages_height; y++) {
			write_data_ssd1306(pgm_read_byte(p++));
		}

		--column;
	}

	return end_col;
}

uint8_t getSymbolWidth_gfx(char c, const char *symbols)
{
	const char ofs = pgm_read_byte(symbols + c);
	const char ofs_ = pgm_read_byte(symbols + c + 1);

	char c_width = ofs_ - ofs;
	return c_width;
}

int16_t drawSymbol_gfx(char c, const char *symbols , unsigned char page, int16_t column, char page_height)
{
	const char ofs = pgm_read_byte(symbols + c);
	const char ofs_ = pgm_read_byte(symbols + c + 1);

	char c_width = ofs_ - ofs;

	const char *p = symbols + ofs;

	int16_t end_col = column + c_width - 1;

	// assumption: 128 column width canvas

	// skip if start, end out of canvas bounds
	if (column <= 127 && end_col >= 0) {

		if (end_col > 127) end_col = 127;

		/// special case of 1-page characters
		if (page_height == 1) {


			// skip columns outside of canvas from left side
			while (column < 0) {
				p++;
				++column;
			}

			// set page and column range
			set_page_range_vh_addressing(page, page);
			set_column_range_vh_addressing(column, end_col);

			while(column <= end_col) {
				write_data_ssd1306(pgm_read_byte(p++));
				++column;
			}
		}
		else {
			set_page_range_vh_addressing(page, page + page_height - 1);

			// skip columns outside of canvas from left side
			while(column < 0) {
				for (char y = 0; y < page_height; y++) {
					pgm_read_byte(p++);
				}

				++column;
			}

			while(column <= end_col) {
				for (char y = 0; y < page_height; y++) {
					write_data_ssd1306(pgm_read_byte(p++));
				}

				++column;
			}

		}


	}

	return end_col + 1;

}


void fillRange_gfx(char c, uint8_t column_start, uint8_t page_start, uint8_t column_end, uint8_t page_end)
{
	set_page_range_vh_addressing(page_start, page_end);
	set_column_range_vh_addressing(column_start, column_end);

	int16_t nb = (page_end - page_start + 1) * (column_end - column_start + 1);

	while (nb--) write_data_ssd1306(c);
}

