//set your clock speed
#define F_CPU 8000000UL
//these are the include files. They are outside the project folder
#include <avr/io.h>
#include <avr/delay.h>
//this include is in your project folder
#include "abc.h"
#include "ssd_1306.h"
#include "display_gfx.h"
#include "FixedSys_font.h"
#include "OcrB_font.h"

void program1();

void program2();

void program3();

void program4();

void program5();

void program6();

int main(void) {
	program6();
}



void program6() {

	setup_atmel_ssd();

	reset_ssd1306();
	set_vcomh_deselect_level_ssd1306(2);//(4);
	set_precharge_period_ssd1306(2,2);//(1, 15);
	set_charge_pump_ssd1306(1);
	on_ssd1306();

	clear_gfx();


	char c[5] = {"*0.0"};
	while (1) {
		clear_gfx();

		for (char i = 0+48; i < 4+48; i++) {
			c[1] = i;

			for (char j = 0+48; j < 10+48; j++) {

				c[3] = j;
				drawString_gfx(c, 1, 0, OCRB29x48s44_ascii42_57, 29, 48, 42);
				delay_ms(50);

			}
		}

	}
}

void program5() {

	setup_atmel_ssd();

	reset_ssd1306();
	set_vcomh_deselect_level_ssd1306(2);//(4);
	set_precharge_period_ssd1306(2,2);//(1, 15);
	set_charge_pump_ssd1306(1);
	on_ssd1306();

	clear_gfx();


	while (1) {

		// 'A' = 65, addr = 578
		//drawGlcdGlyph(font8x16 + 561, 16, 4, 0);

		char col = 0;
		const char page = 0;
		col = drawChar_gfx('A', page, col,  fixedsys8x16, 8, 16, 32);
		col = drawChar_gfx('l', page, col,  fixedsys8x16, 8, 16, 32);
		col = drawChar_gfx('a', page, col,  fixedsys8x16, 8, 16, 32);
		col = drawChar_gfx(' ', page, col,  fixedsys8x16, 8, 16, 32);
		col = drawChar_gfx('m', page, col,  fixedsys8x16, 8, 16, 32);
		col = drawChar_gfx('a', page, col,  fixedsys8x16, 8, 16, 32);
		col = drawChar_gfx(' ', page, col,  fixedsys8x16, 8, 16, 32);
		col = drawChar_gfx('k', page, col,  fixedsys8x16, 8, 16, 32);
		col = drawChar_gfx('o', page, col,  fixedsys8x16, 8, 16, 32);
		col = drawChar_gfx('t', page, col,  fixedsys8x16, 8, 16, 32);
		col = drawChar_gfx('a', page, col,  fixedsys8x16, 8, 16, 32);
		col = drawChar_gfx('.', page, col,  fixedsys8x16, 8, 16, 32);


		drawString_gfx("Line 2-3 *()", 2, 60, fixedsys8x16, 8, 16, 32);

		drawString_gfx("Line 4-5 !@#$%^&*", 4, 0, fixedsys8x16, 8, 16, 32);

		drawString_gfx("Line 6-7 +-=_.,1890", 6, 0, fixedsys8x16, 8, 16, 32);

		delay_ms(5000);

		clear_gfx();

		drawString_gfx("+1.45", 1, 0, OCRB29x48s44_ascii42_57, 29, 48, 42);

		delay_ms(5000);

		drawString_gfx("-1.68", 1, 0, OCRB29x48s44_ascii42_57, 29, 48, 42);

		delay_ms(5000);

		drawString_gfx("+2.99", 1, 0, OCRB29x48s44_ascii42_57, 29, 48, 42);

		delay_ms(5000);

		drawString_gfx("-2.99", 1, 0, OCRB29x48s44_ascii42_57, 29, 48, 42);

		delay_ms(5000);
	}

}


void program2() {
	setup_atmel_ssd();

	while (1) {

		reset_ssd1306();
		set_vcomh_deselect_level_ssd1306(2);//(4);
		set_precharge_period_ssd1306(2,2);//(1, 15);
		set_charge_pump_ssd1306(1);
		on_ssd1306();

		entire_display_on_ssd1306();

		delay_ms(1000);

		entire_display_ram_ssd1306();

		delay_ms(1000);

		set_contrast_ssd1306(0x7F);

		delay_ms(1000);

		set_contrast_ssd1306(0xFF);

		delay_ms(1000);

		char contrast = 0;
		do {
			set_contrast_ssd1306(contrast++);

			delay_ms(50);
		} while (contrast != 0xFF);

		set_inversed_display_ssd1306();

		delay_ms(1000);

		set_non_inversed_display_ssd1306();

		delay_ms(1000);

		set_inversed_display_ssd1306();

		delay_ms(1000);

		set_non_inversed_display_ssd1306();

		delay_ms(1000);

		set_inversed_display_ssd1306();

		delay_ms(1000);

		set_non_inversed_display_ssd1306();

		delay_ms(1000);

		clear_gfx();

		delay_ms(1000);

		fill_pattern_gfx(0xAA, 32, 96, 1, 7);

		delay_ms(1000);

	}
}



void program3() {
	setup_atmel_ssd();

	while (1) {

		reset_ssd1306();
		on_ssd1306();
		clear_gfx();
		fill_pattern_gfx(0xAA, 32, 96, 1, 7);

		delay_ms(1000);

		set_vcomh_deselect_level_ssd1306(0);
		char contrast = 0;
		do {
			set_contrast_ssd1306(contrast++);

			delay_ms(50);
		} while (contrast != 0xFF);

		set_vcomh_deselect_level_ssd1306(2);
		contrast = 0;
		do {
			set_contrast_ssd1306(contrast++);

			delay_ms(50);
		} while (contrast != 0xFF);


		set_vcomh_deselect_level_ssd1306(4);
		contrast = 0;
		do {
			set_contrast_ssd1306(contrast++);

			delay_ms(50);
		} while (contrast != 0xFF);

		delay_ms(1000);

	}
}

void program4() {
	setup_atmel_ssd();

	while (1) {

		reset_ssd1306();
		on_ssd1306();
		clear_gfx();
		fill_pattern_gfx(0xAA, 32, 96, 1, 7);

		set_precharge_period_ssd1306(2, 1);
		char contrast = 0;
		do {
			set_contrast_ssd1306(contrast++);

			delay_ms(50);
		} while (contrast != 0xFF);

		set_precharge_period_ssd1306(2, 5);
		contrast = 0;
		do {
			set_contrast_ssd1306(contrast++);

			delay_ms(50);
		} while (contrast != 0xFF);


		set_precharge_period_ssd1306(2, 10);
		contrast = 0;
		do {
			set_contrast_ssd1306(contrast++);

			delay_ms(50);
		} while (contrast != 0xFF);

		set_precharge_period_ssd1306(2, 15);
		contrast = 0;
		do {
			set_contrast_ssd1306(contrast++);

			delay_ms(50);
		} while (contrast != 0xFF);

		delay_ms(1000);

	}
}


///////////////////////////////////////////////////////

void program1() {

	//Set PORTC to all outputs
	DDRC = 0xFF;

	uint16_t width = 5000;

	uint16_t step = (15 * (width / 1000)) / 20;

	uint16_t t_hi = 0;
	//create an infinite loop
	while (1) {

		uint16_t t_lo = width - t_hi;

		cycle(t_hi, t_lo);

		t_hi = (t_hi + step) % width;
	};

}

void cycle(uint16_t t_hi, uint16_t t_lo) {

	//turns C HIGH
	PORTC = 0xFF;

	delay_us(t_hi);

	// C low
	PORTC = 0x00;

	delay_us(t_lo);
}


///////////////////////////////////////////////////////


void led_test1() {

	uint16_t cycle = 0;
	uint16_t reg = 0x01;

	while (1) {

		if (cycle & 0x20) {

			if (cycle & 0x01) {
				set_led(reg, 0x04); // green NPN
			} else {
				set_led(reg, 0x03); // red PNP
			}
		}
		else {
			if (cycle & 0x01) {
				set_led(reg, 0x01); // white PNP
			} else {
				set_led(reg, 0x02); // white NPN
			}
		}

		if (cycle & 0x01) {
			reg <<= 1;
			if (!reg) {
				reg = 0x01;
			}
		}

		cycle++;
		delay_ms(25);

		printInteger_gfx(cycle, 0, 0, 0);
	}
}
void led_test2() {

	/*
	// enable timer overflow interrupt for both Timer0 and Timer1
    TIMSK=(1<<TOIE0);
	// set timer0 counter initial value to 0
	TCNT0=0x00;
	// start timer0 with /1024 prescaler
	//TCCR0 = (1<<CS02) | (1<<CS00);
	TCCR0 = (1 << CS01) ;//| (1<<CS00);
	// enable interrupts
	sei();

	uint16_t cycle = 0;

	for (int i= 0; i < 16; i++) {
		mainNeedle[i*2] = 0;
		mainNeedle[i*2 + 1] = 0;
		auxNeedle[i] = 0;
		limitsNeedle[i] = 0;
	}

	uint8_t k = 0;
	uint8_t l = 31;

	uint8_t k1 = 0;
	uint8_t l1 = 0;

	while (1) {

	 	mainNeedle[k1] = 0;
	 	mainNeedle[l1] = 0;
	 	mainNeedle[k] = cycle & 0xFF;
	 	mainNeedle[l] = cycle & 0xFF;

	 	auxNeedle[(k1/2 + 8) % 16] = 0;
	 	limitsNeedle[(l1/2 + 8) % 16] = 0;
		auxNeedle[(k / 2 + 8) % 16] =  0xFF;
		limitsNeedle[(l / 2 + 8) % 16] = 0xFF;

	 	auxNeedle[(k1/2 + 4) % 16] = 0;
	 	limitsNeedle[(l1/2 + 4) % 16] = 0;
		auxNeedle[(k / 2 + 4) % 16] =  0x30;
		limitsNeedle[(l / 2 + 4) % 16] = 0x10;

		k1 = k;
		l1 = l;
		k = (k + 1) % 32;
		l = (l - 1) % 32;

		cycle++;
		delay_ms(25);

		//printInteger_gfx(cycle, 0, 0, 0);
	}
*/
}



void led_test3() {

	// enable timer overflow interrupt for both Timer1
    TIMSK=(1<<OCIE1A);
	// set timer1 counter initial value to 0
	TCNT0=0x00;
	//TCCR0 = (1 << CS01); // | */ (1<<CS00);

	TCCR1B |= 1 << WGM12 |  1 << CS10; // 8MHz
	OCR1A = 500;

	// enable interrupts
	sei();


	mainNeedle = 0x00;
	auxNeedle = 0x80808080; //1 << 0 | 1 << 4 | 1 << 8 | 1 << 12 | 1 << 16 | 1 << 20 | 1 << 24 | 1 << 28;

	uint16_t cycle = 0;
	while (1) {

		int h = 16 +  (int)(15.0 * sin(cycle * 3.1415 / 80.0));

		mainNeedle = 1;
		mainNeedle <<= h;

		cycle++;
		delay_ms(10);
		printInteger_gfx(cycle, 0, 0, 0);
		printInteger_uart(cycle);
	}

}

