/*
 * comm_api.h
 *
 *  Created on: 17 lip 2020
 *      Author: krodak
 */

#ifndef COMM_API_H_
#define COMM_API_H_

void comm_setup();
void comm_teardown();

void comm_sendFrame(uint8_t *data, uint8_t size);

#endif /* COMM_API_H_ */
