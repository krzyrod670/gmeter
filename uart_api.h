/*
 * uart_api.h
 *
 *  Created on: 21 mar 2020
 *      Author: kroda
 */

#ifndef UART_API_H_
#define UART_API_H_

void uart_setup();
void uart_teardown();
void uart_transmit(uint8_t data);
void uart_transmitInt(int integer);

// sends array of characters after current transmission is done
void uart_scheduleBulkTransmit(uint8_t* data, uint8_t length);
void uart_continueBulkTransmit();

#endif /* UART_API_H_ */
