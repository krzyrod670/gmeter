/*
 * i2c.h
 *
 * IIC basic communication routines and error state.
 *
 * Error state i2c_errors must be reset prior to performing IIC task.
 *
 * Define I2C_DEBUG to get debug printouts.
 *
 *  Created on: 6 sty 2020
 *  Author: Krzysztof Rodak
 */

#ifndef I2C_H_
#define I2C_H_

//#define I2C_DEBUG

/** ERROR sending START condition */
#define I2C_START_ERROR 0x01;

/** ERROR sending REPEATED-START condition */
#define I2C_REP_START_ERROR 0x02;

/** ERROR sending slave address for writing SLA + W */
#define I2C_SLA_W_ERROR 0x04;

/** ERROR sending slave address for reading SLA + R */
#define I2C_SLA_R_ERROR 0x08;

/** ERROR writing a byte to slave */
#define I2C_WRITE_ERROR 0x10;

/** ERROR reading a byte from slave (with ACK)*/
#define I2C_READ_ERROR 0x20;

/** ERROR reading last byte from slave (with NAK)*/
#define I2C_READ_END_ERROR 0x40;





/** Status of IIC, 0 means no errors.  */
extern volatile uint8_t i2c_errors;



/** Setups I2C on a device */
void i2c_setup();

/** Initiates Master mode transmission, Sends Start condition.
 * May result in I2C_START_ERROR */
void i2c_start();

/** Finalizes Master mode transmission, sends Stop condition.
 * No errors produced. */
void i2c_stop();

/** Re-Initializes Master mode transmission, Sends Repeated Start condition in Master Mode.
 * After that data direction may change, use appr. addressing routine.
 * May result in I2C_SLA_W_ERROR */
void i2c_rep_start();

/**
 * Addresses slave device for writing, SLA + W , in Master Mode. Direction bit is set for writing to slave.
 * May result in I2C_SLA_W_ERROR
 *
 * @Param address Address of slave device, 1 - 127
 */
void i2c_address_sla_w(uint8_t address);

/**
 * Addresses slave device for reading, SLA + R, in Master Mode. Direction bit is set for reading from slave
 * May result in I2C_SLA_R_ERROR
 *
 * @Param address Address of slave device, 1 - 127
 */
void i2c_address_sla_r(uint8_t address);


/** Writes a single byte to slave. Master SLA + W mode.
 * May result in I2C_WRITE_ERROR */
void i2c_write(uint8_t data);

/** Reads a single byte out of multiple from slave device, sends ACK after receiving, Master SLA + R Mode.
 * May result in  I2C_READ_ERROR */
uint8_t i2c_read();

/** Reads a single byte from slave device, the last one, sends NAK after receiving, Master SLA + R Mode
 * May result in  I2C_READ_END_ERROR */
uint8_t i2c_read_end();



#endif /* I2C_H_ */
