/*
 * led_routines.h
 *
 *  Created on: 22 mar 2020
 *      Author: kroda
 */

#ifndef LED_ROUTINES_SPI_H_
#define LED_ROUTINES_SPI_H_

#define LED_SPI

#include <avr/io.h>

#include "spi_api.h"

void setRegister_led(uint16_t reg);
void select_led(uint8_t ctrl);
void latch_led();
void outputEnable_led();
void outputDisable_led();

#define LED_CTRL_PORT PORTD
#define LED_LATCH_CK 6
#define LED_DISABLE 7

void setup_led() {
	DDRD = 0xFB;
	LED_CTRL_PORT = (1 << LED_DISABLE) | (1 << LED_LATCH_CK);

	outputEnable_led();
}

void set_led(uint16_t reg, uint8_t ctrl) {
	// adjust negation if PNP
	if (! (ctrl & 0x01)) {
		reg = ~reg;
	}

	waitTransmitted_spi();

	PORTB = PORTB | 0x02; // OLED's /CS -> High


	setRegister_led(reg);
	select_led(ctrl & 0x07);
	latch_led();

	PORTB = PORTB & 0xFD; // OLED's /CS -> Low
}

void setRegister_led(uint16_t reg) {
	transmit_spi(reg & 0xFF);
	transmit_spi((reg >> 8) & 0xFF);
}

void latch_led() {

	LED_CTRL_PORT = LED_CTRL_PORT | (1 << LED_LATCH_CK);
	delay_us(2);
	LED_CTRL_PORT = LED_CTRL_PORT & (~(1 << LED_LATCH_CK));
}

void outputEnable_led() {
	LED_CTRL_PORT = LED_CTRL_PORT & 0x7F;
}

void outputDisable_led() {
	LED_CTRL_PORT = LED_CTRL_PORT | 0x80;
}

// reg - 16 bits of shift register
// ctrl: bits: 0-2
// 0 x x - all switched off,
// 1 0 0 - white A (negative)
// 1 0 1 - white B (positive)
// 1 1 0 - green (negative)
// 1 1 1 - red (positive)
void select_led(uint8_t line) {

	switch (line) {
	case 1:
		// white A (NPN)
		//
		// White NPN is set on (High)
		// White PNP is set off (High)
		// Green NPN is set off (Low)
		// Red   PNP is set off (High)
		transmit_spi(0xD0);
		break;
	case 2:
		// white B (PNP)
		//
		// White PNP is set on (Low)
		// White NPN is set off (Low)
		// Green NPN is set off (Low)
		// Red   PNP is set off (High)
		transmit_spi(0x10);
		break;
	case 3:
		// green (NPN)
		//
		// White PNP is set off (High)
		// White NPN is set off (Low)
		// Green NPN is set On (High)
		// Red   PNP is set off (High)
		transmit_spi(0xB0);
		break;
	case 4:
		// red (PNP)
		//
		// White PNP is set off (High)
		// White NPN is set off (Low)
		// Green NPN is set Off (Low)
		// Red   PNP is set on (Low)
		transmit_spi(0x80);
		break;
	default:
		// all switched off
		//
		// PNP's are all turned off, bits set High (1)
		// NPN's are all turned off, bits set Low (0)
		transmit_spi(0x90);
		break;
	}
}

#endif /* LED_ROUTINES_SPI_H_ */
