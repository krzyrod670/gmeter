/*
 * comm_routines_uart.h
 *
 *  Created on: 18 lip 2020
 *      Author: kroda
 */

#ifndef COMM_ROUTINES_UART_H_
#define COMM_ROUTINES_UART_H_

#include "comm_api.h"
#include "uart_api.h"

#include <avr/io.h>

void comm_setup()
{
	/* Set baud rate */
	UBRRH = 0x00; //(unsigned char)(MYUBRR>>8);
	//UBRRL = 51; // (unsigned char)MYUBRR; // 9600
	UBRRL = 12; // 38400
	/* Enable receiver and transmitter */
	UCSRB = (1<<RXEN)|(1<<TXEN);
	/* Set frame format: 8data, 2stop bit */
	UCSRC = (1<<URSEL)|(0<<USBS)|(3<<UCSZ0);
}

void comm_teardown()
{

}

void comm_sendFrame(uint8_t *data, uint8_t size)
{
	// data are scheduled to be transmitted after data size is transmitted
	uart_scheduleBulkTransmit(data, size);

	// initiate transmission with frame size
	uart_transmit(size);
}


#endif /* COMM_ROUTINES_UART_H_ */
