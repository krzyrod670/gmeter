/*
 * led_routines.h
 *
 *  Created on: 22 mar 2020
 *      Author: kroda
 */

#ifndef LED_ROUTINES_PORTIO_H_
#define LED_ROUTINES_PORTIO_H_

#include <avr/io.h>

void setRegister_led(uint16_t reg);
void select_led(uint8_t ctrl);
void latch_led();
void resetLatch_led();
void outputEnable_led();
void outputDisable_led();

// PC
#define LED_GREEN_NPN 0
#define LED_RED_PNP 1
#define LED_WHITE_NPN 2
#define LED_WHITE_PNP 3

// PD
#define LED_DISABLE 7

// PB
#define LED_MASTER_RESET 0

// PD
#define LED_CTRL_PORT PORTD
#define LED_LATCH_CK 4
#define LED_SHIFT_CK 5
#define LED_DATA 6

void setup_led() {
	DDRD = 0xFB;
	PORTD = (1 << LED_DISABLE) | (1 << LED_LATCH_CK);

	DDRC = 0x0F;
	PORTC = PORTC | (1 << LED_RED_PNP) | (1 << LED_WHITE_PNP);
	PORTC = PORTC & (~(1 << LED_GREEN_NPN)) & (~(1 << LED_WHITE_NPN));

	outputEnable_led();
}

void set_led(uint16_t reg, uint8_t ctrl) {
	// adjust negation if PNP
	if (!(ctrl & 0x01)) {
		reg = ~reg;
	}


	setRegister_led(reg);

	outputDisable_led();
	select_led(ctrl & 0x07);
	latch_led();
	outputEnable_led();
}

void setRegister_led(uint16_t reg) {
	// adjust order of bytes
	reg = (reg >> 8 & 0x00FF) | reg << 8;

	for (int i = 0; i < 16; i++) {

		// set data & shift clock = 0
		PORTD = (PORTD & (~(1 << LED_DATA)) & (~(1 << LED_SHIFT_CK)))
				| ((reg & 0x8000) >> (15 - LED_DATA));

		delay_ms(1);

		// clock 0->1
		PORTD = PORTD | 1 << LED_SHIFT_CK; // 0 -> 1

		reg <<= 1;
	}
}

void latch_led() {
	LED_CTRL_PORT = LED_CTRL_PORT & (~(1 << LED_LATCH_CK));
	delay_ms(1);
	LED_CTRL_PORT = LED_CTRL_PORT | (1 << LED_LATCH_CK);
}

void outputEnable_led() {
	PORTD = PORTD & 0x7F;
}

void outputDisable_led() {
	PORTD = PORTD | 0x80;
}

// reg - 16 bits of shift register
// ctrl: bits: 0-2
// 0 x x - all switched off,
// 1 0 0 - white A (negative)
// 1 0 1 - white B (positive)
// 1 1 0 - green (negative)
// 1 1 1 - red (positive)
void select_led(uint8_t line) {

	switch (line) {

	// white A (NPN)
	case 1:
		// White NPN is set on (High)
		// White PNP is set off (High)
		// Green NPN is set off (Low)
		// Red   PNP is set off (High)
		PORTC = PORTC | (1 << LED_RED_PNP) | (1 << LED_WHITE_PNP)
				| (1 << LED_WHITE_NPN);
		PORTC = PORTC & (~(1 << LED_GREEN_NPN));
		break;

		// white B (PNP)
	case 2:
		// White PNP is set on (Low)
		// White NPN is set off (Low)
		// Green NPN is set off (Low)
		// Red   PNP is set off (High)
		PORTC = PORTC | (1 << LED_RED_PNP);
		PORTC = PORTC
				& (~((1 << LED_GREEN_NPN) | (1 << LED_WHITE_NPN)
						| (1 << LED_WHITE_PNP)));
		break;

		// green (NPN)
	case 3:
		// White PNP is set off (High)
		// White NPN is set off (Low)
		// Green NPN is set On (High)
		// Red   PNP is set off (High)
		PORTC = PORTC | (1 << LED_RED_PNP) | (1 << LED_WHITE_PNP)
				| (1 << LED_GREEN_NPN);
		PORTC = PORTC & (~(1 << LED_WHITE_NPN));
		break;

		// red (PNP)
	case 4:
		// White PNP is set off (High)
		// White NPN is set off (Low)
		// Green NPN is set Off (Low)
		// Red   PNP is set on (Low)
		PORTC = PORTC | (1 << LED_WHITE_PNP);
		PORTC = PORTC
				& (~((1 << LED_GREEN_NPN) | (1 << LED_WHITE_NPN)
						| (1 << LED_RED_PNP)));
		break;
		// all switched off
	default:
		// PNP's are all turned off, bits set High (1)
		// NPN's are all turned off, bits set Low (0)
		PORTC = PORTC | (1 << LED_RED_PNP) | (1 << LED_WHITE_PNP);
		PORTC = PORTC & (~((1 << LED_GREEN_NPN) | (1 << LED_WHITE_NPN)));
		break;
	}

}

#endif /* LED_ROUTINES_PORTIO_H_ */
