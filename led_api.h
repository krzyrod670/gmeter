/*
 * led_api.h
 *
 *  Created on: 22 mar 2020
 *      Author: kroda
 */

#ifndef LED_API_H_
#define LED_API_H_

void setup_led();

// reg - 16 bits of shift register
// ctrl: bits: 0-2
// 0 x x - all switched off,
// 1 0 0 - white A (negative)
// 1 0 1 - white B (positive)
// 1 1 0 - green (negative)
// 1 1 1 - red (positive)
void set_led(uint16_t reg, uint8_t ctrl);


#endif /* LED_API_H_ */
