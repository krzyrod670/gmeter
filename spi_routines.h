/*
 * spi.c
 *
 *  Created on: 27 pa� 2019
 *      Author: kroda
 */

#ifndef SPI_ROUTINES_H_
#define SPI_ROUTINES_H_

#include <avr/io.h>

#include "spi_api.h"

void setup_spi(void) {

	spi_state = SPI_STATE_UNINITIALIZED & ~SPI_STATE_DIRTY;

	/* Enable SPI, Master, set clock rate fck/2 */
	SPCR = (0<<SPIE)|(1<<SPE)|(0 <<DORD)|(1<<MSTR)|(0<<CPOL)|(0<<CPHA)|(0<<SPR0)|(0<<SPR1);
	SPSR = (1<<SPI2X);

	spi_state &= ~SPI_STATE_UNINITIALIZED; // clear flag
}

void waitTransmitted_spi() {
	if (spi_state & SPI_STATE_DIRTY)
		while (!(SPSR & (1<<SPIF)));
}

void transmit_spi(uint8_t cData) {
	spi_state |= SPI_STATE_DIRTY; // set dirty flag
	SPDR = cData;
	while(!(SPSR & (1<<SPIF)));
}

#endif // SPI_ROUTINES_H_
