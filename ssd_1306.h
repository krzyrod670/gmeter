#define SSD_1306_RES 0
#define SSD_1306_CS 1
#define SSD_1306_DC 2
#define SSD_1306_SCK 5
#define SSD_1306_SDIN 3


#define SSD_1306_COMM_SPI


// basic commands
void on_ssd1306();
void sleep_ssd1306();
void entire_display_on_ssd1306();
void entire_display_ram_ssd1306();
void set_contrast_ssd1306(char level);
void set_inversed_display_ssd1306();
void set_non_inversed_display_ssd1306();

void set_precharge_period_ssd1306(char p1, char p2);
void set_vcomh_deselect_level_ssd1306(char level);
void set_charge_pump_ssd1306(char enabled);


// addressing commands
void setup_page_addressing_ssd1306();
void set_page_pg_addressing_ssd1306(char page);
void set_start_column_pg_addressing_ssd1306(char page);
void setup_horizontal_addressing_ssd1306();
void setup_vertical_addressing_ssd1306();
void set_column_range_vh_addressing(char start_column, char end_column);
void set_page_range_vh_addressing(char start_page, char end_page);

void write_cmd_ssd1306(char cmd);
void write_cmd_2_ssd1306(char cmd_first, char cmd_second);
void write_cmd_3_ssd1306(char cmd_first, char cmd_second, char cmd_third);
void write_data_ssd1306(char dat);
void write_data_array_ssd1306(char* dat, int len);
void comm_ssd1306_spi(char cmd);
void comm_ssd1306_port_io(char cmd);

// hardware
void setup_atmel_ssd();
void control_ssd1306(char dc, char cs, char res);
void control_full_ssd1306(char dc, char cs, char res, char sdin, char sck);
void reset_ssd1306();
