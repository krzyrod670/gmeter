/*
 * needle_api.h
 *
 *  Created on: 31 mar 2020
 *      Author: kroda
 */

#ifndef NEEDLE_API_H_
#define NEEDLE_API_H_

// bit represents a diode
uint32_t mainNeedle;

// bit represents a diode
uint32_t auxNeedle;

void update_needle();

#endif /* NEEDLE_API_H_ */
