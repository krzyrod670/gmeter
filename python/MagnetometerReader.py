'''
Created on 22 lut 2020

@author: kroda
'''
import sys
import serial
import io 
import math

 # configure the serial connections 
ser = serial.Serial(
    port='COM8',
    baudrate=9600,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS
)

ser.close()
ser.open()
ser.isOpen()

cal_sens = [320, 297.5, 300]
cal_ofs = [2082.5, 2055.0, 1872.5]

i = 1
try:
    #read initial line - might be incomplete
    ser.readline()
    while True:       
        
        
        line = ser.readline().decode('utf-8')[:-1]
                
        values = line.split(",")
        #print("line: " + line)
        #print(values);
        
        readings = [int(numeric_string) for numeric_string in values]        
        #print(readings);
        
        m = [0.0, (readings[0] - cal_ofs[0])/cal_sens[0], (readings[1] - cal_ofs[1])/cal_sens[1], (readings[2] - cal_ofs[2])/cal_sens[2]];
        m[0] = 0.496 * math.sqrt(m[1]*m[1] + m[2]*m[2] + m[3]*m[3]);
        nn = ["{:.3f}".format(round(mvv,3)) for mvv in m];        
                
        print(nn);
        
        #
        i = i + 1
except KeyboardInterrupt:
    pass        

ser.close()

print("Done")

    
