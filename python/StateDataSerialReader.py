'''
Created on 22 lut 2020

@author: kroda
'''
import sys
import serial
import io 

def readStateVector(ser, f):
    line = ser.readline().decode('utf-8')[:-1]
    if line:        
        f.write(line + '\n')
        f.flush()

port = sys.argv[1];
fileName = sys.argv[2];

    
print('Using serial port:', port)
print('Output to file: ', fileName)

# configure the serial connections 
ser = serial.Serial(
    port='COM8',
    baudrate=9600,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS
)

ser.close()
ser.open()
ser.isOpen()
    
f = open(fileName, 'w+')
f.write('No,X,Y,Z\n')

i = 1
try:
    #read initial line - might be incomplete
    ser.readline()
    while True:       
        print("Measurement ", i, " CTRL+C to stop")
        #write measurement line number (starting from 1)
        f.write(str(i) + ',')
        #write measurement line and end line
        readStateVector(ser, f)
        #
        i = i + 1
except KeyboardInterrupt:
    pass        

f.close();
ser.close()

print("Done")

    
