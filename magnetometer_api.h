/*
 * magnetometer_api.h
 *
 *  Created on: 21 mar 2020
 *      Author: kroda
 */

#ifndef MAGNETOMETER_API_H_
#define MAGNETOMETER_API_H_

void drawCompassScale();
void drawCompassValues();

#endif /* MAGNETOMETER_API_H_ */
