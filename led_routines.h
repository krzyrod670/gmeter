/*
 * led_routines.h
 *
 *  Created on: 22 mar 2020
 *      Author: kroda
 */

#ifndef LED_ROUTINES_H_
#define LED_ROUTINES_H_

#define LED_SPI

#ifdef LED_SPI
#include "led_routines_spi.h"
#else
#include "led_routines_portio.h"
#endif


#endif /* LED_ROUTINES_H_ */
