/*
 * mxr2312el_api.h
 *
 *  Created on: 18 lip 2020
 *      Author: kroda
 */

#ifndef MXR2312EL_API_H_
#define MXR2312EL_API_H_

typedef uint16_t adc_t;

typedef struct {
	adc_t mxr_x;
	adc_t mxr_y;
	adc_t mxr_temp;
} Mxr2312Sample_t;

void mxr2312_setup();
void mxr2312_teardown();

void mxr2312_getSample(const Mxr2312Sample_t *data);


#endif /* MXR2312EL_API_H_ */
