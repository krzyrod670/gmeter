/*
 * uart_routines.h
 *
 *  Created on: 21 mar 2020
 *      Author: kroda
 */

#ifndef UART_ROUTINES_H_
#define UART_ROUTINES_H_

#include <stdlib.h>
#include <assert.h>
#include <avr/io.h>

#define UART_SUBSYSTEM (1 << 1)


uint8_t *uart_bulkData;
uint8_t uart_bulkRemainingCount;

void uart_setup()
{
	uart_bulkData = 0x00;
	uart_bulkRemainingCount = 0x00;
}

void uart_teardown()
{
}

void uart_transmit(uint8_t data ) {

	/* Wait for empty transmit buffer and/or bulk transfer is complete*/
	while (!(UCSRA & (1 << UDRE)));

	/* Put data into buffer, sends the data */
	UDR = data;
}

void uart_printInt(int integer)
{
	char text[] = "          ";
	char *p = text + 9;

	int k = abs(integer);

	do {
		char x = '0' + (char) (k%10);
		*(p--) = x;

		k = k / 10;
	} while (k);

	if (integer < 0) {
		*(p) = '-';
	} else p++;

	while (*p) {
		uart_transmit(*p);
		p++;
	}
}

void uart_scheduleBulkTransmit(uint8_t* data, uint8_t length)
{
	// wait till other bulk transfer completes
	while (uart_bulkRemainingCount);

	uart_bulkData = data;
	uart_bulkRemainingCount = length;

	// set on the interrupt
	UCSRB = UCSRB | (1 << UDRIE);
}

// shall be invoked on TX buffer empty interrupt
void uart_continueBulkTransmit()
{
	UDR = *(uart_bulkData++);

	if (uart_bulkRemainingCount == 1) {
		// disable the TX buffer empty interrupt
		UCSRB = UCSRB & (~(1 << UDRIE));
	}

	--uart_bulkRemainingCount;
}

#endif /* UART_ROUTINES_H_ */
