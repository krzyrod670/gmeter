/*
 * mxr2312el_routines.h
 *
 *  Created on: 18 lip 2020
 *      Author: kroda
 */

#ifndef MXR2312EL_ROUTINES_H_
#define MXR2312EL_ROUTINES_H_

#include "mxr2312el_api.h"
#include <avr/io.h>

void mxr2312_setup()
{
	ADMUX = 1 << REFS0;
	ADCSRA = (1 << ADEN) | (1 << ADPS2) | (1 << ADPS1);
	// start first ADC conversion
	ADCSRA = ADCSRA | (1 << ADSC);
	while (ADCSRA & (1 << ADSC));
}

void mxr2312_teardown()
{
	ADCSRA = (0 << ADEN) | (1 << ADPS2) | (1 << ADPS1);
}

void mxr2312_getSample(const Mxr2312Sample_t *data)
{
	uint8_t *byteData = (uint8_t*) data;

	// ADC conversion - x
	ADMUX = ADMUX & 0xFC;
	ADCSRA = ADCSRA | (1 << ADSC);
	while (ADCSRA & (1 << ADSC));
	byteData[0] = ADCL;
	byteData[1] = ADCH;

	// ADC conversion - y
	ADMUX = (ADMUX & 0xFC) | 0x01;
	ADCSRA = ADCSRA | (1 << ADSC);
	while (ADCSRA & (1 << ADSC));
	byteData[2] = ADCL;
	byteData[3] = ADCH;

	// ADC conversion - temp
	ADMUX = (ADMUX & 0xFC) | 0x02;
	ADCSRA = ADCSRA | (1 << ADSC);
	while (ADCSRA & (1 << ADSC));
	byteData[4] = ADCL;
	byteData[5] = ADCH;
}

#endif /* MXR2312EL_ROUTINES_H_ */
