/*
 * magnetometer_routines.h
 *
 *  Created on: 21 mar 2020
 *      Author: kroda
 */


#ifndef MAGNETOMETER_ROUTINES_H_
#define MAGNETOMETER_ROUTINES_H_


#define MAGNETOMETER_SUBSYSTEM  (1 << 5)

#include "math.h"


int16_t c_x = 113; // bearing
int16_t c_w = 35; // angle range displayed / 2

int16_t c_ds = 5 ;// angle spacing between ticks
uint8_t c_page = 7; // compass scale page
uint8_t c_page_height = 1; // compass scale page height
const char *c_symbols = busola_sym_pzl_v3; // symbols used in compass scale

int16_t c_dw = 30; // angle spacing between numeric values
uint8_t c_page_w = 3; // page of compass numeric values
uint8_t c_page_height_w = 3; // height in pages, of compass numeric values
const char *c_w_font = OCRB16x24s23_ascii48_57; // font used in compass values scale


const char* const c_values  []  = {
		"<", "1", "2" , "3", "4", "5", "6", "7", "8", ":", "10", "11", "12", "13", "14", "15", "16", "17",
		"=", "19", "20", "21", "22", "23", "24", "25", "26", ";", "28", "29", "30", "31", "32", "33", "34", "35" };



void program7()
{
	delay_ms(100);


	mmc3120_set_coil();

	int16_t ofs_x = 0;
	int16_t ofs_y = 0;
	int16_t ofs_z = 0;

	delay_ms(50);
	mmc3120_initiate_measurement();
	delay_ms(30);
	mmc3120_get_measurement();
	ofs_x = asv.m_x;
	ofs_y = asv.m_y;
	ofs_z = asv.m_z;

	delay_ms(10);
	mmc3120_reset_coil();
	delay_ms(50);

	mmc3120_initiate_measurement();
	delay_ms(30);
	mmc3120_get_measurement();
	ofs_x += asv.m_x;
	ofs_y += asv.m_y;
	ofs_z += asv.m_z;
	ofs_x /= 2;
	ofs_y /= 2;
	ofs_z /= 2;



	mmc3120_initiate_measurement();
	delay_ms(20);

	int cc = 0;
	while(1) {


		mmc3120_get_measurement();
/*
		if (cc <= 180) {
			c_w = 10 + cc / 2;
		}
		else {
			c_w = 185 - cc / 2;
		}

		int k_w = c_w / 2;

		if (k_w <= 7) c_dw = 10;
		else if (k_w <= 25) c_dw = 30;
		else c_dw = 90;

		if (c_w <= 35) {
			c_ds = 5;
		}
		else if (c_w <= 65) {
			c_ds = 10;
		}
		else {
			c_ds = 30;
		}


*/
		drawCompassScale();
		drawCompassValues();


		//drawString_gfx("                                                                  ", 0, 0, 0);
		/*printInteger_gfx(c_x, 0, 0, 0);
		printInteger_gfx(asv.m_x - 2048, 0, 30, 0);
		printInteger_gfx(2048 - asv.m_y, 0, 80, 0);
		*/
		printInteger_uart(asv.m_x);
		transmitUsart(',');
		printInteger_uart(asv.m_y);
		transmitUsart(',');
		printInteger_uart(asv.m_z);
		transmitUsart('\n');

		int xx = asv.m_x - 2083;
		int yy = asv.m_y - 2057 ;

		int f_ang = 360  + (int) ( 180.0 * atan2(xx, -yy) ) / M_PI;

		c_x = ((int)f_ang) % 360;
/*

		c_x = (c_x + 1)%360;

		if (c_x % 5 == 0) {
			cc = (cc + 1)%360;
		}

*/
		mmc3120_initiate_measurement();

		delay_ms(25);
	}

}

void drawCompassScale()
{
	// left scale range  (angle)

	int16_t s0;

	if (c_x >= c_w) {
		s0 = ((c_x - c_w )/ c_ds) * c_ds;
	}
	else {
		s0 = ((c_x - c_w - c_ds + 1)/ c_ds) * c_ds;
	}


	// right scale range (angle)
	int16_t s1 = ((c_x + c_w + c_ds - 1)/ c_ds) * c_ds;

	// total number of drawn items "ticks"
	uint8_t n = (s1 - s0)/ c_ds + 1;

	// s - current scale point (angle)
	//int16_t s = s0;
	int16_t s = s1;

	// temporary for calculating next p
//	int16_t pp = (s - (c_x - c_w) ) * 64 ;//+ c_w/2; // 64 = 1/2 display width
	int16_t pp = ((c_x + c_w) - s) * 64 ;//+ c_w/2; // 64 = 1/2 display width


	int16_t p = 0; // current column (X) on display (pixels)
	for (uint8_t i = 0; i < n; i++) {

		// select symbol on scale -> z
		char z = 0;
		if (s % 30 == 0) z = 0;
		else if (s % 10 == 0) z = 1;
		else if (s % 5 == 0) z = 2;

		// get width of symbol
		int8_t zw =  getSymbolWidth_gfx(z, c_symbols);

		// calculate position of symbol on display
		int16_t p_ =  pp / c_w  - zw/2;

		if (i == 0) {

			if ( p_ + zw >= 0) {
				p = drawSymbol_gfx(z, c_symbols, c_page, p_, c_page_height);
			}

		}
		else if (i == 1) {

			if ( p_ > 0) {
				fillRange_gfx(0x00, p, c_page, p_ - 1, c_page);
			}

			p = drawSymbol_gfx(z, c_symbols, c_page, p_, c_page_height);

		}
		else if (i == n - 2) {

			fillRange_gfx(0x00, p, c_page, p_ - 1, c_page);
			p = drawSymbol_gfx(z, c_symbols, c_page, p_, c_page_height);
		}
		else if (i == n - 1) {
			if (p < 128) {

				if (p_ < 128) {
					fillRange_gfx(0x00, p, c_page, p_ - 1, c_page);
					p = drawSymbol_gfx(z, c_symbols, c_page, p_, c_page_height);
				}
				else {
					fillRange_gfx(0x00, p, c_page, 127, c_page);
				}
			}
		}
		else {

			fillRange_gfx(0x00, p, c_page, p_ - 1, c_page);
			p = drawSymbol_gfx(z, c_symbols, c_page, p_, c_page_height);
		}

		pp += c_ds * 64;

		//s += c_ds;
		s -= c_ds;
	}

}

void drawCompassValues()
{
	// left scale range  (angle)

	setFont_gfx(OCRB16x24s23_ascii48_57, 16, 24, 48);

	int16_t s0;

	if (c_x >= c_w) {
		s0 = ((c_x - c_w )/ c_dw) * c_dw;
	}
	else {
		s0 = ((c_x - c_w - c_dw + 1)/ c_dw) * c_dw;
	}

	// right scale range (angle)
	int16_t s1 = ((c_x + c_w + c_dw - 1)/ c_dw) * c_dw;

	// total number of drawn items "ticks"
	uint8_t n = (s1 - s0)/ c_dw + 1;

	// s - current scale point (angle)
	//int16_t s = s0;
	int16_t s = s1;


	// temporary for calculating next p
	//int16_t pp = (s - (c_x - c_w) ) * 64 ;//+ c_w/2; // 64 = 1/2 display width
	int16_t pp = ((c_x + c_w) - s ) * 64 ;//+ c_w/2; // 64 = 1/2 display width


	int16_t p = 0; // current column (X) on display (pixels)
	for (uint8_t i = 0; i < n; i++) {

		// select symbol on scale -> z
		const char* z = c_values[((s + 360) % 360) / 10];

		// get width of symbol
		int8_t zw = getStringWidth_gfx(z);

		// calculate position of symbol on display
		int16_t p_ =  pp / c_w  - zw/2;

		if (i == 0) {

			if ( p_ + zw >= 0) {
				p = drawString_gfx(z, c_page_w, p_, 0);
			}

		}
		else if (i == 1) {

			if ( p_ > 0) {
				fillRange_gfx(0x00, p, c_page_w, p_ - 1, c_page_w + c_page_height_w - 1);
			}

			p = drawString_gfx(z, c_page_w, p_, 0);

		}
		else if (i == n - 2) {

			fillRange_gfx(0x00, p, c_page_w, p_ - 1,  c_page_w + c_page_height_w - 1);
			p = drawString_gfx(z, c_page_w, p_, 0);
		}
		else if (i == n - 1) {
			if (p < 128) {

				if (p_ < 128) {
					fillRange_gfx(0x00, p, c_page_w, p_ - 1,  c_page_w + c_page_height_w - 1);
					p = drawString_gfx(z, c_page_w, p_, 0);
				}
				else {
					fillRange_gfx(0x00, p, c_page_w, 127,  c_page_w + c_page_height_w - 1);
				}
			}
		}
		else {
			fillRange_gfx(0x00, p, c_page_w, p_ - 1,  c_page_w + c_page_height_w - 1);
			p = drawString_gfx(z, c_page_w, p_, 0);
		}

		pp += c_dw * 64;

		//s += c_dw;
		s -= c_dw;
	}

	revertFont_gfx();

}




#endif /* MAGNETOMETER_ROUTINES_H_ */

