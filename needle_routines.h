/*
 * needle_routines.c
 *
 *  Created on: 31 mar 2020
 *      Author: kroda
 */


#ifndef NEEDLE_ROUTINES_H_
#define NEEDLE_ROUTINES_H_

#include "needle_api.h"
#include "led_api.h"

uint8_t needlePhase = 0;

uint16_t prev_led_reg = 0;


void update_needle()
{
	if (needlePhase == 0) {

		uint32_t m = mainNeedle;

		uint16_t led_reg = 0x0000;
		for (int i = 0; i < 16; i++) {
			led_reg <<= 1;
			led_reg |= m & 0x0001;
			m >>= 2;
		}

		// output LED
		set_led(led_reg, 1);
		prev_led_reg = led_reg;
	}
	else if (needlePhase == 8) {

			uint32_t m = auxNeedle;

			uint16_t led_reg = 0x0000;
			for (int i = 0; i < 16; i++) {
				led_reg <<= 1;
				led_reg |= m & 0x0001;
				m >>= 2;
			}

			led_reg |= prev_led_reg;

			// output LED
			set_led(led_reg, 1);
	}
	else if (needlePhase == 9) {

		// pre-switch off - let outputs of registers be stabilized
		set_led(0xFFFF, 0);
	}

	else if (needlePhase == 10) {

		uint32_t m = mainNeedle >> 1;

		uint16_t led_reg = 0x0000;
		for (int i = 0; i < 16; i++) {
			led_reg <<= 1;
			led_reg |=  m & 0x0001;
			m >>= 2;
		}

		// output LED
		set_led(led_reg, 2);
		prev_led_reg = led_reg;
	}
	else if (needlePhase == 18) {

			uint32_t m = auxNeedle >> 1;

			uint16_t led_reg = 0x0000;
			for (int i = 0; i < 16; i++) {
				led_reg <<= 1;
				led_reg |= m & 0x0001;
				m >>= 2;
			}

			led_reg |= prev_led_reg;

			// output LED
			set_led(led_reg, 2);
	}


	else if (needlePhase == 19) {

		// pre-switch off - let outputs of registers  be stabilized
		set_led(0x0000, 0);
	}

	else if (needlePhase == 21) {

		// switch LEDs off for the remaining time of cycle
		set_led(0x0000, 1);
	}

	needlePhase = (needlePhase + 1) % 200;
}



#endif /* NEEDLE_ROUTINES_H_ */
